# Backend

### How it works!

##### Stack

```
Docker
Node.js 8.10
MongoDB
PM2
```

##### Requirements

```
Docker
Docker-compose
Postman
```

##### Install npm dependencies

Go to the application folder(api/) and install all the Node.js dependecies

```sh
npm install
```

##### Environment

To set the environment and get everthing running automatically, use the command line, go to the folder where is docker-compose.yml(root/) file and run

```sh
docker-compose up (-d optional)
```
To get inside of debian container, where is the node application running:

example:

```sh
docker exec -ti backendapi_app_1 bash
```

##### Application

The application is started with the container using the file ecosystem.config.js.

Logs:

```sh
pm2 logs
```

Status:

```sh
pm2 status
```

Start:

```sh
pm2 start ecosystem.json
```

Stop:

```sh
pm2 restart ecosystem.json
```

Restart:

```sh
pm2 restart ecosystem.json
```

Delete:

```sh
pm2 delete all
```

#### Token Authentication:

Usage of some API's endpoints require token authentication, that it is provided after each successfull login.

Endpoints that require token, it must be passed in the request header:

| key            | value |
| -------------- | ----- |
| x-access-token | token |

### Features

Personnaly I recommend to use Postman app to test the API endpoints.

Please do not forget to set the following header:

| key          | value |
| ------------ | ----- |
| Content-Type | application/json |

### Documentation

To access the documentation just go to

```sh
cd dist/
```

and open the HTML file index.html with firefox.