const express = require('express');

const { integrate } = require('../middlewares/swagger-middleware');

// Start database connection
require('./database');

const app = express();

const check = require('../routes/check');
const auth = require('../routes/authentication');
const users = require('../routes/users');
const fileManager = require('../routes/file-manager');

const application = async () => {
  try {
    await integrate(app, './dist/spec.yml');
  } catch (error) {
    throw new Error('Loading swagger failed.')
  }

  app.use('/welcome', check);
  app.use('/users', users);
  app.use('/login', auth);
  app.use('/file', fileManager);

  return app;
};

module.exports = application;
