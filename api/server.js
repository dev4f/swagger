// const express = require('express');

// const { integrate } = require('./middlewares/swagger-middleware');
// const auth = require('./routes/authentication');
// const users = require('./routes/users');

const application = require('./config/app');

const init = async () => {
  const app = await application();

  app.use((err, req, res, next) => {
    res.send(err.message);
  });

  const port = process.env.PORT || 8443;
  app.listen(port, () => {
    console.log(`Listening to the port: ${port}`);
  });
};

init();
