const AWS = require('aws-sdk');

const bucket = process.env.BUCKET_NAME || 'aluxion.bucket/tests';
const s3 = new AWS.S3();

const uploadFile = (content, filename) => {
  const params = { Bucket: bucket, Key: `filipe/${filename}`, Body: content };
  return s3.upload(params, (err) => {
    if (err) {
      throw Error(`File upload failed ${err}`);
    }
  });
};

const downloadFile = (filename) => {
  const params = { Bucket: bucket, Key: `filipe/${filename}` };
  return s3.getSignedUrl('getObject', params);
};

const deleteFile = (key) => {
  const params = { Bucket: bucket, Key: `filipe/${key}` };
  return s3.deleteObject(params).promise();
};

const changeFileName = async (oldKey, newKey) => {
  await s3.copyObject({ Bucket: bucket, CopySource: `${bucket}/filipe/${oldKey}`, Key: `filipe/${newKey}` }).promise();
  await deleteFile(oldKey);
};

module.exports = {
  uploadFile,
  downloadFile,
  deleteFile,
  changeFileName,
};
