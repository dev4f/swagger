const bcrypt = require('bcryptjs');

const User = require('../models/Users');

const createUser = async (body) => {
  const { name, email, password } = body;
  const hashPassword = bcrypt.hashSync(password, 8);

  const user = new User(
    {
      name,
      email,
      password: hashPassword,
    },
  );

  return user.save();
};

const getAllUser = async () => User.find();

const getUser = async (query) => {
  const one = await User.findOne(query);

  if (!one) throw new Error('User not found');

  return one;
};

const resetPass = async (id, newPassword) => {
  const newhashedPassword = bcrypt.hashSync(newPassword, 8);
  return User.updateOne({ _id: id }, { password: newhashedPassword });
};

module.exports = {
  createUser,
  getAllUser,
  getUser,
  resetPass,
};
