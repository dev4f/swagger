const jwt = require('jsonwebtoken');
const request = require('supertest');

const app = require('../config/app');
const secret = require('../config/configs');

jest.mock('../config/database.js');
jest.mock('../controllers/user-controller.js');
jest.mock('../controllers/file-controller.js');

describe('Test lantivirus scenarios for data coming from S3 Bucker', () => {
  test('Check endpoint should return 200', async () => {
    const moveon = await app();
    const response = await request(moveon).get('/welcome');
    expect(response.statusCode).toBe(200);
    expect(response.body).toEqual('Welcome to the Api');
  });
  test('Try to login without credentials should return 500', async () => {
    const moveon = await app();
    const response = await request(moveon).post('/login');
    expect(response.statusCode).toBe(500);
  });
  test('Register with right data should return 200.', async () => {
    const moveon = await app();
    const response = await request(moveon).post('/users/registration').send({ name: 'test', email: 'test', password: 'teste' });
    expect(response.statusCode).toBe(200);
    expect(response.body).toEqual('User created.');
  });
  test('Register without required data should return 400', async () => {
    const moveon = await app();
    const response = await request(moveon).post('/users/registration').send({ name: 'test', email: 'test' });
    expect(response.statusCode).toBe(400);
  });
  test('Upload file without auth should return 403', async () => {
    const moveon = await app();
    const response = await request(moveon).post('/file/upload');
    expect(response.statusCode).toBe(403);
    expect(response.body).toEqual('No token provided.');
  });
  test('Delete file with wrong token should return 500', async () => {
    const moveon = await app();
    const response = await request(moveon).post('/file/delete').set({ 'x-access-token': 'teste' }).send({ filename: 'test' });
    expect(response.statusCode).toBe(500);
    expect(response.body).toEqual('Failed to authenticate token.');
  });
  test('Delete file with check token should return 200', async () => {
    const token = jwt.sign({ id: '1234' }, secret, {
      expiresIn: 60,
    });
    const moveon = await app();
    const response = await request(moveon).post('/file/delete').set({ 'x-access-token': token }).send({ filename: 'test' });
    expect(response.statusCode).toBe(200);
    expect(response.body).toEqual('File deleted.');
  });
});
