const createMiddleware = require('swagger-express-middleware');

const integrate = (app, spec) => new Promise((resolve, reject) => {
  createMiddleware(spec, app, (error, middleware) => {
    if (error) {
      reject(error);
      return;
    }

    app.use(
      middleware.metadata(),
      middleware.CORS(),
      middleware.files(),
      middleware.parseRequest(),
      middleware.validateRequest(),
    );

    resolve();
  });
});

module.exports = {
  integrate,
};
