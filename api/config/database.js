const mongoose = require('mongoose');

const mongoDB = 'mongodb://172.24.0.3:27017/crud';
mongoose.connect(mongoDB, { useNewUrlParser: true });

const db = mongoose.connection;

db.on('error', (e) => {
  console.log('MongoDB connection error ', e);
});
db.once('open', () => {
  console.log('Connection to database has been successful.');
});
