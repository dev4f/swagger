const userController = require('./user-controller.js');
const fileController = require('./file-controller.js');

module.exports = {
  userController,
  fileController,
};
