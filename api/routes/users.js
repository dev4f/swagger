const router = require('express').Router();

const { userController } = require('../controllers');

router.post('/registration', async ({ body }, res) => {
  try {
    await userController.createUser(body);
  } catch (error) {
    res.status(400).json('User creation failed.');
    return;
  }
  res.json('User created.');
});

router.post('/reset', async ({ body }, res) => {
  try {
    const { userId, newPassword } = body;
    await userController.resetPass(userId, newPassword);
  } catch (error) {
    res.status(400).json('User password reset failed');
    return;
  }
  res.json('Password updated.');
});

module.exports = router;
