const mongoose = require('mongoose');

const { Schema } = mongoose;

const FileSchema = new Schema({
  key: {
    type: String,
    required: true,
    max: 100,
  },
  ownerId: {
    type: String,
    trim: true,
  },
});

// Export the model
module.exports = mongoose.model('File', FileSchema);
