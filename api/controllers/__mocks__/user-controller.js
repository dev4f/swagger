const createUser = async (body) => {
  const { name } = body;
  if (name !== 'test') throw new Error();
  return {};
};

module.exports = {
  createUser,
};
