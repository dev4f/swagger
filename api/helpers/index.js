const fs = require('fs');
const { promisify } = require('util');

const readFileAsync = promisify(fs.readFile);

const getImageToBase64 = async path => readFileAsync(path);

module.exports = {
  getImageToBase64,
};
