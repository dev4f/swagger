const express = require('express');
const multer = require('multer');

const { fileController } = require('../controllers');
const { getImageToBase64 } = require('../helpers');
const verifyToken = require('../middlewares/verifyToken');

const router = express.Router();
const upload = multer({ dest: 'tmp/' });

router.post('/upload', verifyToken, upload.single('file'), async (req, res) => {
  try {
    const { originalname, path } = req.files.file;
    const content = await getImageToBase64(path);
    const key = `${req.userId}/${originalname}`;
    await fileController.uploadFile(content, key);
    res.status(200).json(`File stored as: ${originalname}`);
  } catch (e) {
    res.status(500).json(`Internal Server Error ${e}`);
  }
});

router.post('/download', verifyToken, async (req, res) => {
  const key = `${req.userId}/${req.body.filename}`;
  const link = await fileController.downloadFile(key);
  res.status(200).json(`Url: ${link}`);
});

router.post('/changeFileName', verifyToken, async (req, res) => {
  const oldKey = `${req.userId}/${req.body.oldName}`;
  const newKey = `${req.userId}/${req.body.newName}`;
  try {
    await fileController.changeFileName(oldKey, newKey);
  } catch (error) {
    res.status(400).json('File not found.');
    return;
  }
  res.json('File name changed.');
});

router.post('/delete', verifyToken, async (req, res) => {
  const key = `${req.userId}/${req.body.filename}`;
  try {
    await fileController.deleteFile(key);
  } catch (error) {
    res.status(400).json('File not deleted.');
    return;
  }
  res.json('File deleted.');
});

module.exports = router;
