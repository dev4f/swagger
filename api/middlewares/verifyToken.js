const jwt = require('jsonwebtoken');

const secret = require('../config/configs');

function verifyToken(req, res, next) {
  const token = req.headers['x-access-token'];
  if (!token) {
    res.status(403).json('No token provided.');
    return;
  }

  jwt.verify(token, secret, (err, decoded) => {
    if (err) {
      res.status(500).json('Failed to authenticate token.');
      return;
    }

    // if everything good, save to request for use in other routes
    req.userId = decoded.id;
    next();
  });
}

module.exports = verifyToken;
