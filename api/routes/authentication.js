const express = require('express');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const { userController } = require('../controllers');
const secret = require('../config/configs');

const router = express.Router();

router.post('/', async ({ body }, res) => {
  try {
    const { name, password, email } = body;
    if (name && password && email) {
      const user = await userController.getUser({ email });
      if (user) {
        const passwordIsValid = bcrypt.compareSync(password, user.password);
        if (!passwordIsValid) {
          return res.status(401).json('Password does not match');
        }

        const token = jwt.sign({ id: user.id }, secret, {
          expiresIn: 86400, // expires in 24 hours
        });

        res.status(200).json({
          message: 'Login successfully.',
          token,
        });
      } else {
        res.status(400).json('Credential does not match.');
      }
    } else {
      res.status(400).json('Authentication process has found an error.');
    }
  } catch (e) {
    res.status(500).json(`Error ${e}`);
  }
});

module.exports = router;
